/*
 * Copyright (c) 2021, Codepoint Technologies, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
*  "uripkg": "//hub.cpflex.tech/ocm/ocs/temperature",
*    "tag": "v1.0",
*    "title": "Temperature sensor type specification",
*    "dateCreated": "2021-11-24T04:15:40:00.000Z",
*    "dateModified": "${__ISODATE__}",
*/

syntax = "proto2";
//import "google/protobuf/descriptor.proto";
import "common.proto";
package Temperature;


/**
* Indicates the triggering mode.
*/
enum TriggerMode {

    //Trigger when absolute temperature is greater than specified value.
    ABSGT=0;
    
    //Trigger when absolute temperature is less than specified value.
    ABSLT=1;

    //Trigger when relative temperature changes (positive or negative) since last report.
    DELTA=2;

    //Trigger when relative temperature changes positively since last report.
    DELTAPOS=3;

    //Trigger when relative temperature changes negiatively since last report.
    DELTANEG=4;
}

/**
* Defines the notification information for triggered temperature events.
*/
message TriggerInfo {
    //The unique trigger identifier, used when reporting temperature events.
    required uint32 id = 1; 

    //Trigger ID that generated the event.
    required uint32 trigger = 2; 

    //The observed temperature value in Celsius.
    required sint32 temperature = 3; 

    //Observed temperature uncertainty in degrees Celsius.
    required uint32 uncertainty = 4;
}

/**
* Reports current temperature information.
*/
message ReportInfo {
    // The unique temperature sensor identifier within a device.
    required uint32 id = 1; 

    //The observed temperature value in Celsius.
    required sint32 temperature = 2; 

    //Observed temperature uncertainty in degrees Celsius.
    required uint32 uncertainty = 3;
}

/**
* Defines a threshold trigger that results in notification event.
*/
message TriggerSpec {
    //The unique trigger identifier, used when reporting temperature events.
    required uint32 id = 1; 

    //The absolute or delta change trigger threshold value. Units are Celsius.
    required uint32 threshold = 2; 

    //Trigger hysteresis to prevent repeated notifications. Units are Celsius.
    //Default value is 2.
    optional uint32 hysteresis = 3; 

    //How notifications events are triggered.
    required TriggerMode mode = 4;
}

/**
* Configures temperature sensor for triggers, notifications and regular statistics reporting.
*/
message Configuration {
    // The unique temperature sensor identifier within a device.
    required uint32 id = 1; 

    // Configurable notification triggers.
    repeated TriggerSpec triggers = 2;

    // The temperature reporting interval.  Set to 0 (default) if disabled. 
    // Units are minutes. Maximum value is 63360  minutes (44 days).
    // Default value is 0 (disabled).
    optional uint32 reportInterval = 3 [default=0];

    // The temperature statistics reporting interval.  Set to 0 (default) if disabled.
    // Units are minutes. Maximum value is 63360  minutes (44 days).
    // Default value is 0 (disabled)
    optional uint32 statsReportInterval = 4 [default=0];

    // The temperature sampling interval.  Set to 0 (default) if disabled. This must be
    // > 0 to enable triggers and statistics functions to work.  If disabled (0),  triggers and 
    // statistics are also disabled. Maximum sampling interval is 1,900,800 seconds (22 days).
    // Units are seconds.
    // default is 0 (disabled).
    optional uint32 samplingInterval = 5 [default=0];

    // If true, triggered notification uplinks are confirmed.
    // Default value is true.
    optional bool confirmTrigger = 6 [default=true];

    // If true, confirm statistics reporting uplink messages.
    // Default value is false.
    optional bool confirmStats = 7 [default=false];
}

/**
* Temperature statistics information reported at regular interval or upon request.
*/
message Statistics {
    // The unique temperature sensor identifier within a device.
    // Units are Celsius.
    required uint32 id = 1;   

    // The minimum reported temperature in the last period.
    // Units are Celsius.
    required sint32 minTemp = 2;

    // The maximum maximum reported temperature in the last period.
    // Units are Celsius.
    required sint32 maxTemp = 3;
    // The average reported temperature in the last period.
    // Units are Celsius.
    required sint32 avgTemp = 4;

    // The calculated variance (take square root to get std. deviation).
    //Units are Celsius.
    required sint32 variance = 5;
    
    // The count of samples in the last period.
    // Units are Celsius.
    required uint32 count = 6;
}

/**
* Temperature Sensor Service Interface Implementation.
*/
service Sensor{
    /**
    * Notification fired whenever trigger threshold exceeded.
    */
    rpc TriggerNotifyEvent(TriggerInfo) returns(VoidMsg);

    /**
    * Temperature (Celsius) report fired at regular intervals.
    */
    rpc ReportEvent( ReportInfo) returns (VoidMsg);

    /** 
    * Temperature statistics report fired at regular intervals.
    */
    rpc StatsReportEvent( Statistics) returns (VoidMsg);

    /**
    * Gets the current configuration with the specified sensor ID.
    */
    rpc GetConfig(Uint32Param) returns (Configuration);

    /**
    * Sets the current configuration.
    */
    rpc SetConfig(Configuration ) returns (Configuration);

    /**
    * Gets the current temperature object statistics with the specified sensor ID.
    */
    rpc GetStats(Uint32Param) returns(Statistics);

    /**
    * Clears the current clear object statistics with the specified sensor ID.
    */
    rpc StatsClear(Uint32Param) returns(VoidMsg);
}
