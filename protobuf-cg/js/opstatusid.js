// source: otaprotocol.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Ota.OpStatusId');

/**
 * @enum {number}
 */
proto.Ota.OpStatusId = {
  COMPLETE: 0,
  CANCELLED: 1,
  ERROR: 2,
  IMAGE_UP_TO_DATE: 3,
  TRANSFER_COMPLETE: 4,
  BUSY: 5
};

