// source: bimodal.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

goog.provide('proto.Bimodal.EventId');

/**
 * @enum {number}
 */
proto.Bimodal.EventId = {
  UNSET: 0,
  SET: 1,
  SET_TIMEOUT: 2
};

