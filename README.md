![Cora Logo](img/cora-logo.png)

# Untethered Water Leak Sensor SDK - CS1010-UL

Information and examples for integrating the CS1010-UL untethered LoRaWAN device.  

The CS1010 leak sensor provides real time reporting of leak detection events.   The sensor implements Codepoint's standard untethered application protocol using 
Google Protocol Buffers (protobuf) to encode and decode the messages communicated between the device and host application.   The protobuf specifications can be found at
[./defs/protobuf](defs/protobuf/README.md).  Generated source code is provided for C++, java, Javascript (JS), and Python.  Use the protoc code generator to generate packages for other languages.   

## Functional Model

The logical interaction model for the CS1010-UL is shown below.

![Cora Untethered Protocol](img/CS1010-UL-Protocol.png)

The applications interact with the CS1010-UL using the Cora Untethered Device Protocol, which organizes interaction into functional services as defined 
in the protobuff specifications.  The services for the CS1010-UL are as follows:

- **Device Settings** � General device configuration and notification messages.
- **Bimodal Sensors** � 2-State sensor messages(Set, Unset); the leak sensor is a bimodal sensor having leak detected (set) or leak clear (unseet) states. Includes
  messges for configuration, statistics, and notification.  
- **Temperature Sensors** � Temperature sensing messages supporting configuration, events, measurements and statistics.
- **OTA Firmware Support** � Future capability to send firmware updates.

The device provides the bimodal leak sensor (id=0) and a temperature sensor (id=0) (+- 2-degree Celsius accuracy), which can be configured to support
a wide variety of applications. The device is configured by default to notify leak detection (set) and leak clear (unset) events. It will also sends a health check notification every 3 hours.  For simple applications, no further configuration may be required.  

However, the device can be configured to do much more.  Using the configuration messages application providers can configure
the device to provide periodic statistics, temperature data, and other notifications.   

## Application Integration

The CS1010-UL implements LoRaWAN v1.0.3, which is supported by a wide variety of LoRaWAN network providers.  As shown in the diagram 
below,  the device communicates with an application via the LoRaWAN network and gateways.   Typically, the application implements
an encoder/decoder to translate the proprietary device messges into a data structure compatible with the application.   

![CS1010 Encoder / Decoder](img/CS1010-UL-Encoder.png)

For CS-1000-UL devices, this can be accomplished by decoding uplinked messages or encoding downlink messages using the code generated protobuf code
to convert to/from the binary wireline encoding into message objects.   From there, the application provider can easily translate
the data into a format compatible with their system. Depending on the network provider, it may also be possible to implement the message
encoding and decoding functions at the network application server.  For example, The Things Network (TTN) has such capabilities.

The specific design and details for integrating the CS1010-UL are mostly dependent on the application architecture and out of scope for this SDK.  This
SDK provides examples on implementing encoding/decoding functions as well as providing various test cases and stimulus to accelerate
integration.


## CS1010-UL Messages
Programmer's have a variety of language options available to process CS-1000 messages.  Conceptually, each Protobuf service interaction defined in the .proto files
are mapped to a specific port on the CS1010.  While the defined interaction is not strictly RPC, it does conform with event, request/response semantics.  Where 
noted in the .proto specifications, events are outbound messages with only the request portion of the service interaction defined.  For savvy developers,
the defined interactions could be mapped easly to gRPC if desired. 

The following sub-sections define the LoRaWAN port mappings to the Protobuff interaction for each of the services implemented
in the device.

### [Device Settings (BatteryDevice.proto)](defs/protobuf/batterydevice.proto)
The BatteryDevice service provides interactions related to general device health and configuration.  The table below shows the LoRaWAN port mappings for 
the specified interaction.  See the protobuff file for detailed descriptions of the messages used to communicate.

| Port (Hex) | Port (Dec.) | Interaction Type | Name (Device.)   | Description                           |
|:----------:|:-----------:|:----------------:|------------------|---------------------------------------|
|    0x10    |      16     |       Event      | HealthCheckEvent | Periodic health check event           |
|    0x11    |      17     |       Event      | ResetEvent       | Device reset notification event       |
|    0x12    |      18     | Request/Response | GetConfig        | Gets the current Device configuration |
|    0x13    |      19     | Request/Response | SetConfig        | Sets the device configration          |


### [Bimodal Leak/Flooding Sensor (bimodal.proto)](defs/protobuf/bimodal.proto)
The CS1010-UL uses the bimodal service interaction to report and configure the leak sensor.  It is identifed
as bidomal sensor #0 (id=0).  Bimodal events and request/response interactions will all identify the leak 
sensor state changes as id=0.  

The state of the reed sensor is mapped to the bimodal sensor state in the following table.

| Leak State  |  Bimodal State |
|:------------------:|:--------------:|
|       Clear        |      Unset     |
|      Detected      |       Set      |

When the water is detected on one of the probes the bimodal sensor will report 'set'.  When the probes are dry,  the bimodal sensor will report 'clear'.  

The table below shows the LoRaWAN port mappings for the bimodal sensor service.  See the protobuff file for detailed descriptions 
of the messages used to communicate.

| Port (Hex) | Port (Dec.) | Interaction Type | Name (Bimodal.Sensor.) | Description                            |
|:----------:|:-----------:|:----------------:|:----------------------:|----------------------------------------|
|    0x20    |      32     |       Event      |       NotifyEvent      | Notifies of a bimodal state event.     |
|    0x21    |      33     |       Event      |    StatsReportEvent    | Stats reported at regular intervals.   |
|    0x22    |      34     | Request/Response |        GetConfig       | Gets the bimodal sensor configuration. |
|    0x23    |      35     | Request/Response |        SetConfig       | Sets the bimodal sensor configuration. |
|    0x22    |      36     | Request/Response |        GetStats        | Gets the current bimodal statistics.   |
|    0x23    |      37     | Request/Response |       StatsClear       | Clears the current bimodal statistics. |


The bimodal sensor can be configured to report a 'set timeout', reminder notification if a leak is not addressed within a specified time period.   See the 'intervalTimeout' and 
countTimeout fields in the Configuration message.  

To regularly report statistics, configure the 'statsInterval' to report at the desired interval.  Maximum report interval 
is 49 days (70,560 minutes).  Additionally, notificdation events and stat messages can be marked as confirmed to ensure delivery.   

### [Temperature Sensor (temperature.proto)](defs/protobuf/temperature.proto)

The CS1010-UL has a coarse resolution (~1.6 C) calibrated temperature sensor useful for monitoring significant changes in temperature.  The sensor provides functions
for interval reporting, absolute/relative triggered threshold notifications, or statistics.   The table below shows the LoRaWAN port mappings for the Temperature service
interactions.  A maximum of six (6) triggers can be configured see performance note below for further limitations.


| Port (Hex) | Port (Dec.) | Interaction Type | Name (Temperature.Sensor.) | Description                               |
|:----------:|:-----------:|:----------------:|:--------------------------:|-------------------------------------------|
|    0x30    |      48     |       Event      |     TriggerNotifyEvent     | Notifies of configured trigger events.    |
|    0x31    |      49     |       Event      |         ReportEvent        | Reports temperature at regular intervals. |
|    0x32    |      50     |       Event      |      StatsReportEvent      | Statistics reported at regular intervals. |
|    0x33    |      51     | Request/Response |          GetConfig         | Gets the sensor configuration.            |
|    0x34    |      52     | Request/Response |          SetConfig         | Sets the sensor configuration.            |
|    0x35    |      53     | Request/Response |          GetStats          | Gets the temperature statistics.          |
|    0x36    |      54     | Request/Response |         StatsClear         | Clears the temperature statistics.        |

#### Peformance Note
Configurations with more than three (3) triggers defined will exceed the minimum datarate (for U.S. it is DR1) payload size restrictions of 51 bytes.
In these cases, the GetConfig/SetConfig responses will not uplink. The only option is to get closer to a gateway so it will communicate at 
at higher data rate.   The maximum size of the configuration can be up to 85 bytes so take that into account when planning deployment.  
If low data rate deployments are required, it is recommended that a  maximum of three (3) triggers be used.   This will allow the
configurationto be packed into the 51 byte message in most cases.


### [OTA Firmware Update (otaprotocol.proto)](defs/protobuf/otaprotocol.proto)

OTA not currently supported, it will be available in future versions of the CS1010-UL.

## Example Downlinks

Sample downlinks for configuring the various leak sensor services are available in the postman collection found here
[CS1010-UL-TTNv3-Test.postman_collection.json](test/postman/CS1010-UL-TTNv3-Test.postman_collection.json).  Be sure to look at the documentation 
for each postman they provide additional information about the payload specification as well as the response.

The postmans are set up to send downlinks via TTN V3.0.  The authentication and device ID will have to be updated for 
particular CS1010-UL device to send.   

To create a custom payload:

1) Generate the encoded protobuf and convert to base-64 encoding.
2) Paste into the payload field of the appropriate postman. Recommend duplicating the postman before modifying.
3) Send the message.  
4) Watching the TTN console, you can see the downlink waiting to go and then monitor the responses.

Similar functions are also available with other network providers.



## Tools and Sample Code

 -- Work in progress, check back later --.


## Firmware Errata Summary
| Issue # | Versions affected | Summary|
|:--------------:|:-----------:|:-------------|
| 1 | < v1.0.7.2 | Inconsistent temperature sampling with low battery and simultaneous LoRa uplinks

#### #1 Inconsistent temperature sampling
This issue is fixed in all firmware releases v1.0.7.2 or higher.

All firmware releases prior to v1.0.7.2 (2022/05/12) exhibit inconsistent temperature sampling measurement (typical > +10C), when the LoRa radio is being utilized simultaneously for uplinks.
Temperature swing will become higher as battery voltage becomes lower over time.  Devices with >75% battery charge may not exhibit any symptoms.

*Note: Factory default device configuration does NOT enable Temperature Module Temperature Sampling. 
 Only advanced use of Temperature Statistics or Temperature-based trigger features are effected by this errata!  Temperature reporting data included in HealthCheck uplinks is not affected by this Errata.*   

Inaccurate temperature results can be encountered consistently if the Temperature Sampling interval is on a time-boundary with Temperature Reporting, Temperature Statistics Reporting, or any other periodic LoRa uplink behavior such as health-check or external sensor stimulus.

Work arounds:
- Do not implement any Temperature Sampling related features (Statistics or Triggers).
- Do not implement configurations where the Temperature Sampling Interval and Temperature Reports or Temperature Statistics Reports can occur within ~1 second of each other.  For example, a Sampling Interval of 10 seconds and a Stats Reporting Interval of 60 seconds may cause every 6th sample to exhibit the error.
- Restrict device to fresh batteries or force battery change notifications at 75%.

## License

Copyright 2021 Codepoint Technologies, Inc.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
and associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO 
EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER 
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
USE OR OTHER DEALINGS IN THE SOFTWARE.
